package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ToDoTaskPage {
	private final String TEXT_NEWTASK = "new_task";
	private final String BUTTON_ADDTASK = ".//*[@id='new_task']/../span";
	private final String BUTTON_MYTASK = "//a[contains(@href, '/tasks')]";
	private final String INPUT_SUBTASK_NAME = "new_sub_task";
	private final String INPUT_SUBTASK_DATE = "dueDate";
	private final String BUTTON_ADD_SUBTASK = "add-subtask";
	private final String BUTTON_CLOSE_SUBTASK = "//button[contains(.,'Close')]";
	
	
	private WebDriver driver;
	private WebDriverWait wait;
	
	public ToDoTaskPage(WebDriver driver) {
		this.driver = driver;
	}
	
	public void clickMyTask() {
		wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(BUTTON_MYTASK)));
		driver.findElement(By.xpath(BUTTON_MYTASK)).click();
	}
	
	public void inputTaskName(String name) throws InterruptedException {
		wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id(TEXT_NEWTASK)));
		driver.findElement(By.id(TEXT_NEWTASK)).sendKeys(name);
	}
	
	public void clickAddTask() {
		driver.findElement(By.xpath(BUTTON_ADDTASK)).click();
	}
	
	public String getNameByLinkText(String name) {
		return driver.findElement(By.linkText(name)).getText();
	}
	
	public void clickManageSubTask(String name) {
		wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//a[contains(text(),'" + name + "')]/../../td[4]/button")));
		driver.findElement(By.xpath(".//a[contains(text(),'" + name + "')]/../../td[4]/button")).click();
	}
	
	public void inputSubTaskName(String subName) {
		driver.findElement(By.id(INPUT_SUBTASK_NAME)).sendKeys(subName);
	}
	
	public void inputSubTaskDate(String subDate) {
		driver.findElement(By.id(INPUT_SUBTASK_DATE)).sendKeys(subDate);
	}
	
	public void clickAddSubTask() {
		driver.findElement(By.id(BUTTON_ADD_SUBTASK)).click();
	}
	
	public void clickCloseModalPopUp() {
		driver.findElement(By.xpath(BUTTON_CLOSE_SUBTASK)).click();
	}
	
}

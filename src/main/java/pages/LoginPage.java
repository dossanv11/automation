package pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;



public class LoginPage {
	
	private final String BUTTON_SIGNIN = "/html/body/div[1]/div[1]/div/div[2]/ul[2]/li[1]/a";
	private final String INPUT_EMAIL = "user_email";
	private final String INPUT_PASS = "user_password";
	private final String BUTTON_ENTER = "commit"; //By name
	private final String LOGIN_SUCCESS = ".//*[@class='container']/div[2]";
	
	private WebDriver driver;
	
	public LoginPage(WebDriver driver) {
		this.driver = driver;
	}
	
	public void clickSignIn() {
		driver.findElement(By.xpath(BUTTON_SIGNIN)).click();
	}
	
	public void inputEmail(String email) {
		driver.findElement(By.id(INPUT_EMAIL)).sendKeys(email);
	}
	
	public void inputPass(String pass) {
		driver.findElement(By.id(INPUT_PASS)).sendKeys(pass);
	}
	
	public void clickEnter() {
		driver.findElement(By.name(BUTTON_ENTER)).click();
	}
	
	public void validLoginSuccess() {
		Assert.assertEquals("Signed in successfully.", driver.findElement(By.xpath(LOGIN_SUCCESS)).getText());
	}

}

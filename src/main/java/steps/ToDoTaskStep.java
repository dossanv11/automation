package steps;

import java.util.List;

import action.ToDoTaskAction;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ToDoTaskStep {
	
	private List<List<String>> dataTable;
	private final ToDoTaskAction tdta = new ToDoTaskAction();
	
	@Given("^I am logged on ToDoApp website$")
	public void iAmLoggedOnToDoAppWebsite(DataTable dt) throws Throwable {
	    dataTable = dt.raw();
	    tdta.goToWebPage();
	    tdta.logIn(dataTable.get(1).get(0), dataTable.get(1).get(1));
	    dataTable = null;
	}

	@When("^I input a task name$")
	public void iInputATaskName(DataTable dt) throws Throwable {
	    dataTable = dt.raw();
	    tdta.addNewTask(dataTable.get(1).get(0));
	}

	@Then("^I verify if task was added$")
	public void iVerifyIfTaskWasAdded() throws Throwable {
		tdta.validateTaskName(dataTable.get(1).get(0));
		tdta.endSession();
		dataTable = null;
	}

	@When("^I input an invalid name$")
	public void iInputAnInvalidName(DataTable dt) throws Throwable {
		dataTable = dt.raw();
		tdta.addNewTask(dataTable.get(1).get(0));
		dataTable = null;
	}

	@Then("^I verify the requirement message$")
	public void iVerifyTheRequirementMessage() throws Throwable {
		tdta.validateInvalidTaskName();
		tdta.endSession();
	}
	
	
	@When("^I open the modal popup$")
	public void iOpenTheModalPopup(DataTable dt) throws Throwable {
		dataTable = dt.raw();
		tdta.accessSubTasks(dataTable.get(1).get(0));
	}

	@When("^I input the valid datas$")
	public void iInputTheValidDatas() throws Throwable {
		tdta.addDataSubTask(dataTable.get(1).get(1), dataTable.get(1).get(2));
	}

	@Then("^I verify if the subtask was added$")
	public void iVerifyIfTheSubtaskWasAdded() throws Throwable {
		tdta.validateSubTaskName(dataTable.get(1).get(1));
		tdta.closeModalPopPup();
		dataTable = null;
		tdta.endSession();
	}
	
	@When("^I add a subtask with name and without a date$")
	public void iAddASubtaskWithNameAndWithoutADate() throws Throwable {
		tdta.inputJustNameSubTask(dataTable.get(1).get(1));
	}

	@Then("^I validate the message error missing require date field$")
	public void iValidateTheMessageErrorMissingRequireDateField() throws Throwable {
		dataTable = null;
		tdta.validateInvalidTaskName();
		tdta.endSession();
	}
	
	@When("^I add a subtask with date and without a name$")
	public void iAddASubtaskWithDateAndWithoutAName() throws Throwable {
		tdta.inputJustDateSubTask(dataTable.get(1).get(2));
	}

	@Then("^I validate the message error missing require name field$")
	public void iValidateTheMessageErrorMissingRequireNameField() throws Throwable {
		dataTable = null;
		tdta.validateMissingSubTaskDate();
		tdta.endSession();
	}
	
}

package action;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import pages.LoginPage;
import pages.ToDoTaskPage;

public class ToDoTaskAction {
	
	private WebDriver driver;
	//private ToDoTaskPage tdtp = new ToDoTaskPage(driver);
	private LoginPage lp;
	private ToDoTaskPage tp;
	
	public void goToWebPage() {
		System.setProperty("webdriver.chrome.driver",
				"\\\\"+ System.getProperty("user.dir") +"\\drivers\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://qa-test.avenuecode.com");
		lp = new LoginPage(driver);
		tp = new ToDoTaskPage(driver);
	}

	public void endSession() {
		driver.quit();
	}
	
	@Test
	public void logIn(String email, String pass) {
		tp.clickMyTask();
		lp.inputEmail(email);
		lp.inputPass(pass);
		lp.clickEnter();
		lp.validLoginSuccess();
	}
	
	
	public void goToMyTask() {
		//tdtp = new ToDoTaskPage(driver);
		tp.clickMyTask();
	}
	
	public void addNewTask(String name) throws InterruptedException {
		tp.inputTaskName(name);
		tp.clickAddTask();
	}
	
	public void validateTaskName(String name) {
		Assert.assertEquals(name, tp.getNameByLinkText(name));
	}
	
	public void validateInvalidTaskName() {
		Assert.assertEquals("Input a valid name for your task. The name must contain more than 3 letters", "");
	}
	
	public void accessSubTasks(String name) {
		tp.clickManageSubTask(name);
	}
	
	public void addDataSubTask(String subName, String subDate) {
		tp.inputSubTaskName(subName);
		tp.inputSubTaskDate(subDate);
		tp.clickAddSubTask();
	}
	
	public void validateSubTaskName(String subName) {
		Assert.assertEquals(subName, tp.getNameByLinkText(subName));
	}
	
	public void closeModalPopPup() {
		tp.clickCloseModalPopUp();
	}
	
	public void inputJustDateSubTask(String subDate) {
		tp.inputSubTaskDate(subDate);
		tp.clickAddSubTask();
	}
	
	public void inputJustNameSubTask(String subName) {
		tp.inputSubTaskName(subName);
		tp.clickAddSubTask();
	}
	
	public void validateMissingSubTaskDate() {
		Assert.assertEquals("Due Date value is missing. Please, insert a date", "");
	}
	
	public void validateMissingSubTaskName() {
		Assert.assertEquals("SubTask name value is missing. Please, insert a name", "");
	}
	
}

@TaskTest
Feature: Testing ToDoApp Task
  This feature will test the creation of tasks.

  Background: Open the website and login
  	Given I am logged on ToDoApp website
    |Email												|Password		|
    |valdir.santos001@yahoo.com.br|Av3nue@2019|
  
  @validTask
  Scenario: Test adding a new valid task
  When I input a task name
  |Task Name								|
  |Do my university homework|
  Then I verify if task was added
  
  #This test is failing because of the Bug opened
  @invalidTask @BugOpened
  Scenario: Test adding short name tasks
	When I input an invalid name
	|Task name|
	|it				|
	Then I verify the requirement message
	
	@validSubTask
	Scenario: Test adding a valid new SubTask
	When I open the modal popup
	|Task Name								|SubTask							|Date			 |
  |Do my university homework|This is a new subtask|03/10/2019|
	And I input the valid datas
	Then I verify if the subtask was added
	
	#This test is failing because of the Bug opened
	@invalidSubTaskName @BugOpened
	Scenario: Test adding invalid subtasks
	When I open the modal popup
	|Task Name								|SubTask							|Date			 |
  |Do my university homework|This is a new subtask|03/10/2019|
	And I add a subtask with name and without a date
	Then I validate the message error missing require date field
	
	#This test is failing because of the Bug opened
	@invalidSubTaskDate @BugOpened
	Scenario: Test adding invalid subtasks
	When I open the modal popup
	|Task Name								|SubTask							|Date			 |
  |Do my university homework|This is a new subtask|03/10/2019|
	And I add a subtask with date and without a name
	Then I validate the message error missing require name field
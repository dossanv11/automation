$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("ValidaLogin.feature");
formatter.feature({
  "line": 1,
  "name": "Testando login",
  "description": "",
  "id": "testando-login",
  "keyword": "Feature"
});
formatter.scenario({
  "line": 4,
  "name": "Fazendo login",
  "description": "",
  "id": "testando-login;fazendo-login",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 3,
      "name": "@primeiroTeste"
    }
  ]
});
formatter.step({
  "line": 5,
  "name": "I am on facebook website homepage",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "I enter the data",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "I should see my first page",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginSteps.i_am_on_facebook_website_homepage()"
});
formatter.result({
  "duration": 57341891694,
  "status": "passed"
});
formatter.match({
  "location": "LoginSteps.i_enter_the_data()"
});
formatter.result({
  "duration": 33902305749,
  "status": "passed"
});
formatter.match({
  "location": "LoginSteps.i_should_see_my_first_page()"
});
formatter.result({
  "duration": 31573,
  "status": "passed"
});
});